import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import styles from './my-button.css';
import {Button} from 'react-native-elements';

const MyButton = (props) => {
  return (
    <Button title={props.title} style={styles.button} onPress={props.onPress} />
    // <TouchableOpacity style={styles.button} onPress={props.customClick}>
    //   <Text style={styles.text}>{props.title}</Text>
    // </TouchableOpacity>
  );
};

export default MyButton;
