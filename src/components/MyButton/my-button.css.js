import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#1877f2',
    color: '#ffffff',
    padding: 10,
    // margin: 10,
    marginTop: 16,
    marginLeft: 35,
    marginRight: 35,
  },
  text: {
    color: '#ffffff',
  },
});
