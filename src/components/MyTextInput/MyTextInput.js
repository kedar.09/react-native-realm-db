import React from 'react';
import {View, TextInput} from 'react-native';
import styles from './my-textinput.css';
import {Input} from 'react-native-elements';

const MyTextInput = (props) => {
  return (
    <View style={styles.textInput}>
      {/* <TextInput
        underlineColorAndroid="transparent"
        placeholder={props.placeholder}
        placeholderTextColor="gray"
        keyboardType={props.keyboardType}
        onChangeText={props.onChangeText}
        returnKeyType={props.returnKeyType}
        numberOfLines={props.numberOfLines}
        multiline={props.multiline}
        onSubmitEditing={props.onSubmitEditing}
        style={props.style}
        blurOnSubmit={false}
        value={props.value}
      /> */}
      <Input
        placeholder={props.placeholder}
        errorStyle={props.errorStyle}
        onChangeText={props.onChangeText}
        errorMessage={props.errorMessage}
        value={props.value}
        onBlur={props.onBlur}
      />
    </View>
  );
};

export default MyTextInput;
