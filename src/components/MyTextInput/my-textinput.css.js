import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  textInput: {
    marginLeft: 35,
    marginRight: 35,
    marginTop: 10,
    borderColor: 'gray',
    borderWidth: 1,
  },
});
