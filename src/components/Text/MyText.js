import React from 'react';
import {Text} from 'react-native';
import styles from './my-text.css';

const MyText = (props) => {
  return <Text style={styles.text}>{props.text}</Text>;
};

export default MyText;
