/*Screen to register the user*/
import React, {useState} from 'react';
import {View, ScrollView, SafeAreaView, Alert} from 'react-native';

import MyButton from '../../components/MyButton/MyButton';
import MyTextInput from '../../components/MyTextInput/MyTextInput';
import Realm from 'realm';
import styles from './add-new-user.css';
import * as yup from 'yup';
import {Formik} from 'formik';

const realm = new Realm({path: 'UserDatabase.realm'});

const AddUser = ({navigation}) => {
  const [state, setState] = useState({
    userName: '',
    userContact: '',
    userAddress: '',
  });

  return (
    <SafeAreaView style={styles.addUserSafeAreaView}>
      <View style={styles.addUserViewPage}>
        <View style={styles.addUserSafeAreaView}>
          <ScrollView keyboardShouldPersistTaps="handled">
            <Formik
              enableReinitialize={true}
              initialValues={state}
              onSubmit={(values) => {
                for (let i = 0; i < 1000; i++) {
                  realm.write(() => {
                    var ID =
                      realm.objects('user_details').sorted('user_id', true)
                        .length > 0
                        ? realm
                            .objects('user_details')
                            .sorted('user_id', true)[0].user_id + 1
                        : 1;
                    realm.create('user_details', {
                      user_id: ID,
                      user_name: values.userName,
                      user_contact: values.userContact,
                      user_address: values.userAddress,
                    });
                  });
                }
                Alert.alert(
                  'Success',
                  'User added successfully',
                  [
                    {
                      text: 'Ok',
                      onPress: () => navigation.navigate('HomeScreen'),
                    },
                  ],
                  {cancelable: false},
                );
                // console.log(values);
              }}
              validationSchema={yup.object().shape({
                userName: yup
                  .string()
                  .required('Name is required')
                  .min(3, 'Name must be at least 3 characters')
                  .max(100, 'Enter valid Name'),
                userContact: yup
                  .number()
                  .typeError("Doesn't seems like contact number")
                  .positive("Contact Number can't start with a minus")
                  .integer("Contact Number can't include decimal number")
                  .min(1, 'Enter valid contact number')
                  .max(9999999999, 'Enter valid contact number')
                  .required('Contact Number is required'),
                userAddress: yup
                  .string()
                  .required('Address is required')
                  .min(3, 'Address must be at least 3 characters')
                  .max(200, 'Enter valid Address'),
              })}>
              {(props) => (
                <View>
                  <MyTextInput
                    placeholder="Enter Name"
                    style={styles.addUserTextInput}
                    errorMessage={
                      props.errors.userName && props.touched.userName
                        ? props.errors.userName
                        : null
                    }
                    value={props.values.userName}
                    errorStyle={styles.inputErrorStyle}
                    onBlur={props.handleBlur('userName')}
                    onChangeText={props.handleChange('userName')}
                  />
                  <MyTextInput
                    placeholder="Enter Contact No"
                    style={styles.addUserTextInput}
                    // errorMessage={props.errors.userContact}
                    errorMessage={
                      props.errors.userContact && props.touched.userContact
                        ? props.errors.userContact
                        : null
                    }
                    value={props.values.userContact}
                    onBlur={props.handleBlur('userContact')}
                    errorStyle={styles.inputErrorStyle}
                    onChangeText={props.handleChange('userContact')}
                  />
                  <MyTextInput
                    placeholder="Enter Address"
                    style={styles.addUserTextInput}
                    // errorMessage={props.errors.userAddress}
                    errorMessage={
                      props.errors.userAddress && props.touched.userAddress
                        ? props.errors.userAddress
                        : null
                    }
                    value={props.values.userAddress}
                    onBlur={props.handleBlur('userAddress')}
                    errorStyle={styles.inputErrorStyle}
                    onChangeText={props.handleChange('userAddress')}
                  />
                  <View style={styles.buttonView}>
                    <MyButton title="Submit" onPress={props.handleSubmit} />
                  </View>
                </View>
              )}
            </Formik>
          </ScrollView>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default AddUser;
