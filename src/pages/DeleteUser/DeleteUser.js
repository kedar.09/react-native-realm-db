/*Screen to delete the user*/
import React from 'react';
import {SafeAreaView, View, Alert} from 'react-native';

import MyButton from '../../components/MyButton/MyButton';
import MyTextInput from '../../components/MyTextInput/MyTextInput';
import Realm from 'realm';
import styles from './delete-user.css';
import * as yup from 'yup';
import {Formik} from 'formik';
const realm = new Realm({path: 'UserDatabase.realm'});

const DeleteUser = ({navigation}) => {
  return (
    <SafeAreaView style={styles.deleteUserSafeAreaView}>
      <View style={styles.deleteUserViewPage}>
        <View style={styles.deleteUserSafeAreaView}>
          <Formik
            enableReinitialize={true}
            initialValues={{inputUserId: ''}}
            onSubmit={(values) => {
              realm.write(() => {
                if (
                  realm
                    .objects('user_details')
                    .filtered('user_id =' + values.inputUserId).length > 0
                ) {
                  realm.delete(
                    realm
                      .objects('user_details')
                      .filtered('user_id =' + values.inputUserId),
                  );
                  var user_details = realm.objects('user_details');
                  console.log(user_details);
                  Alert.alert(
                    'Success',
                    'User deleted successfully',
                    [
                      {
                        text: 'Ok',
                        onPress: () => navigation.navigate('HomeScreen'),
                      },
                    ],
                    {cancelable: false},
                  );
                } else {
                  Alert.alert('Please insert a valid User Id');
                }
              });
            }}
            validationSchema={yup.object().shape({
              inputUserId: yup
                .number()
                .typeError("Doesn't seems like User ID")
                .positive("User ID can't start with a minus")
                .integer("User ID can't include decimal number")
                .min(1, 'Enter valid User ID')
                .max(9999999999, 'Enter valid User ID')
                .required('User ID is required'),
            })}>
            {(props) => (
              <View>
                <MyTextInput
                  errorMessage={
                    props.errors.inputUserId && props.touched.inputUserId
                      ? props.errors.inputUserId
                      : null
                  }
                  value={props.values.inputUserId}
                  onBlur={props.handleBlur('inputUserId')}
                  errorStyle={styles.inputErrorStyle}
                  onChangeText={props.handleChange('inputUserId')}
                  placeholder="Enter User Id"
                  style={styles.viewUserTextInput}
                />
                <View style={styles.buttonView}>
                  <MyButton title="Delete User" onPress={props.handleSubmit} />
                </View>
              </View>
            )}
          </Formik>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default DeleteUser;
