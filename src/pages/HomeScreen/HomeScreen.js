/*Home Screen With buttons to navigate to diffrent options*/
import React, {useEffect} from 'react';
import {View, SafeAreaView} from 'react-native';
import MyButton from '../../components/MyButton/MyButton';
import MyText from '../../components/MyText/MyText';
import Realm from 'realm';
let realm;
import styles from './home-screen.css';

const HomeScreen = (props) => {
  useEffect(() => {
    realm = new Realm({
      path: 'UserDatabase.realm',
      schema: [
        {
          name: 'user_details',
          properties: {
            user_id: {type: 'int', default: 0},
            user_name: 'string',
            user_contact: 'string',
            user_address: 'string',
          },
        },
      ],
    });
  }, []);

  return (
    <SafeAreaView style={styles.homeScreenSafeAreaView}>
      <View style={styles.homeScreenViewPage}>
        <View style={styles.homeScreenSafeAreaView}>
          <MyText text="Realm Database" />

          <View style={styles.buttonView}>
            <MyButton
              title="Add New User"
              onPress={() => props.navigation.navigate('AddUser')}
            />
          </View>
          <View style={styles.buttonView}>
            <MyButton
              title="Update User"
              onPress={() => props.navigation.navigate('UpdateUser')}
            />
          </View>
          <View style={styles.buttonView}>
            <MyButton
              title="View User"
              onPress={() => props.navigation.navigate('ViewUser')}
            />
          </View>
          <View style={styles.buttonView}>
            <MyButton
              title="View All User"
              onPress={() => props.navigation.navigate('ViewAllUser')}
            />
          </View>
          <View style={styles.buttonView}>
            <MyButton
              title="View Users List Between Range"
              onPress={() => props.navigation.navigate('ViewUsersRange')}
            />
          </View>
          <View style={styles.buttonView}>
            <MyButton
              title="Delete User"
              onPress={() => props.navigation.navigate('DeleteUser')}
            />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default HomeScreen;
