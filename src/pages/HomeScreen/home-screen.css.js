import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  homeScreenSafeAreaView: {
    flex: 1,
  },
  homeScreenViewPage: {
    flex: 1,
    backgroundColor: 'white',
  },
  buttonView: {
    margin: 20,
  },
});
