import React, {useState} from 'react';
import {View, ScrollView, Alert, SafeAreaView} from 'react-native';
import MyButton from '../../components/MyButton/MyButton';
import MyTextInput from '../../components/MyTextInput/MyTextInput';
import Realm from 'realm';
import styles from './update-user.css';
import * as yup from 'yup';
import {Formik} from 'formik';
const realm = new Realm({path: 'UserDatabase.realm'});

const UpdateUser = ({navigation}) => {
  const [state, setState] = useState({});

  const updateAllStates = (id, name, contact, address) => {
    setState({
      ...state,
      userId: id,
      userName: name,
      userContact: contact.toString(),
      userAddress: address,
    });
  };

  const clearDataAndBack = () => {
    setState({
      ...state,
      userId: '',
      userName: '',
      userContact: '',
      userAddress: '',
    });
  };

  return (
    <SafeAreaView style={styles.updateUserSafeAreaView}>
      <View style={styles.updateUserViewPage}>
        <View style={styles.updateUserSafeAreaView}>
          <ScrollView keyboardShouldPersistTaps="handled">
            {!state.userName ? (
              <Formik
                enableReinitialize={true}
                initialValues={{inputUserId: ''}}
                onSubmit={(values) => {
                  var user_details = realm
                    .objects('user_details')
                    .filtered('user_id =' + values.inputUserId);
                  console.log(user_details);
                  if (user_details.length > 0) {
                    updateAllStates(
                      values.inputUserId,
                      user_details[0].user_name,
                      user_details[0].user_contact,
                      user_details[0].user_address,
                    );
                  } else {
                    Alert.alert('No user found');
                    updateAllStates('', '', '', '');
                  }
                }}
                validationSchema={yup.object().shape({
                  inputUserId: yup
                    .number()
                    .typeError("Doesn't seems like User ID")
                    .positive("User ID can't start with a minus")
                    .integer("User ID can't include decimal number")
                    .min(1, 'Enter valid User ID')
                    .max(9999999999, 'Enter valid User ID')
                    .required('User ID is required'),
                })}>
                {(props) => (
                  <View>
                    <MyTextInput
                      errorMessage={
                        props.errors.inputUserId && props.touched.inputUserId
                          ? props.errors.inputUserId
                          : null
                      }
                      value={props.values.inputUserId}
                      onBlur={props.handleBlur('inputUserId')}
                      errorStyle={styles.inputErrorStyle}
                      onChangeText={props.handleChange('inputUserId')}
                      placeholder="Enter User Id"
                      style={styles.updateUserTextInput}
                    />
                    <View style={styles.buttonView}>
                      <MyButton
                        title="Search User"
                        onPress={props.handleSubmit}
                      />
                    </View>
                  </View>
                )}
              </Formik>
            ) : (
              <Formik
                enableReinitialize={true}
                initialValues={state}
                onSubmit={(values) => {
                  realm.write(() => {
                    var ID = state.userId;
                    console.log('ID', ID);
                    var obj = realm
                      .objects('user_details')
                      .filtered('user_id =' + values.userId);
                    console.log('obj', obj);
                    if (obj.length > 0) {
                      obj[0].user_name = values.userName;
                      obj[0].user_contact = values.userContact;
                      obj[0].user_address = values.userAddress;
                      Alert.alert(
                        'Success',
                        'User updated successfully',
                        [
                          {
                            text: 'Ok',
                            onPress: () => navigation.navigate('HomeScreen'),
                          },
                        ],
                        {cancelable: false},
                      );
                    } else {
                      alert('User Updation Failed');
                    }
                  });
                  // console.log(inputUserId);
                }}
                validationSchema={yup.object().shape({
                  userName: yup
                    .string()
                    .required('Name is required')
                    .min(3, 'Name must be at least 3 characters')
                    .max(100, 'Enter valid Name'),
                  userContact: yup
                    .number()
                    .typeError("Doesn't seems like contact number")
                    .positive("Contact Number can't start with a minus")
                    .integer("Contact Number can't include decimal number")
                    .min(1, 'Enter valid contact number')
                    .max(9999999999, 'Enter valid contact number')
                    .required('Contact Number is required'),
                  userAddress: yup
                    .string()
                    .required('Address is required')
                    .min(3, 'Address must be at least 3 characters')
                    .max(200, 'Enter valid Address'),
                })}>
                {(props) => (
                  <View>
                    <MyTextInput
                      placeholder="Enter Name"
                      style={styles.addUserTextInput}
                      errorMessage={
                        props.errors.userName && props.touched.userName
                          ? props.errors.userName
                          : null
                      }
                      value={props.values.userName}
                      errorStyle={styles.inputErrorStyle}
                      onBlur={props.handleBlur('userName')}
                      onChangeText={props.handleChange('userName')}
                    />
                    <MyTextInput
                      placeholder="Enter Contact No"
                      style={styles.addUserTextInput}
                      // errorMessage={props.errors.userContact}
                      errorMessage={
                        props.errors.userContact && props.touched.userContact
                          ? props.errors.userContact
                          : null
                      }
                      value={props.values.userContact}
                      onBlur={props.handleBlur('userContact')}
                      errorStyle={styles.inputErrorStyle}
                      onChangeText={props.handleChange('userContact')}
                    />
                    <MyTextInput
                      placeholder="Enter Address"
                      style={styles.addUserTextInput}
                      // errorMessage={props.errors.userAddress}
                      errorMessage={
                        props.errors.userAddress && props.touched.userAddress
                          ? props.errors.userAddress
                          : null
                      }
                      value={props.values.userAddress}
                      onBlur={props.handleBlur('userAddress')}
                      errorStyle={styles.inputErrorStyle}
                      onChangeText={props.handleChange('userAddress')}
                    />
                    <View style={styles.buttonView}>
                      <MyButton
                        title="Update User"
                        onPress={props.handleSubmit}
                      />
                    </View>

                    <View style={styles.buttonView}>
                      <MyButton
                        title="Clear and Back"
                        onPress={clearDataAndBack}
                      />
                    </View>
                  </View>
                )}
              </Formik>
            )}
          </ScrollView>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default UpdateUser;
