/*Screen to view all the user*/
import React, {useEffect, useState} from 'react';
import {FlatList, SafeAreaView, Text, View} from 'react-native';
import Realm from 'realm';
import styles from './view-all-user.css';
let realm;

const ViewAllUser = () => {
  let [flatListItems, setFlatListItems] = useState([]);

  useEffect(() => {
    realm = new Realm({path: 'UserDatabase.realm'});
    var user_details = realm.objects('user_details');
    setFlatListItems(user_details);
  }, []);

  let listViewItemSeparator = () => {
    return <View style={styles.viewAllUserListViewItemSeparator} />;
  };

  let listItemView = (item) => {
    return (
      <View key={item.user_id} style={styles.viewAllUserListViewItem}>
        <Text>Id: {item.user_id}</Text>
        <Text>Name: {item.user_name}</Text>
        <Text>Contact: {item.user_contact}</Text>
        <Text>Address: {item.user_address}</Text>
      </View>
    );
  };
    return (
    <SafeAreaView style={styles.viewAllUserSafeAreaView}>
      <View style={styles.viewAllUserViewPage}>
        <View style={styles.viewAllUserSafeAreaView}>
          <FlatList
            data={flatListItems}
            ItemSeparatorComponent={listViewItemSeparator}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => listItemView(item)}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ViewAllUser;
