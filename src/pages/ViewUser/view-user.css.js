import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  viewUserViewPage: {
    backgroundColor: 'white',
    flex: 1,
  },
  viewUserSafeAreaView: {
    flex: 1,
  },
  viewUserTextInput: {
    padding: 10,
  },
  viewUserData: {
    marginLeft: 35,
    marginRight: 35,
    marginTop: 10,
  },
  buttonView: {
    margin: 20,
  },
  inputErrorStyle: {
    color: 'red',
  },
});
