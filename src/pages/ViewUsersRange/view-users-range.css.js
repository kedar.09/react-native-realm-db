import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  viewAllUserListViewItemSeparator: {
    height: 0.2,
    width: '100%',
    backgroundColor: '#808080',
  },
  viewAllUserListViewItem: {
    backgroundColor: 'white',
    padding: 20,
  },
  viewAllUserSafeAreaView: {
    flex: 1,
  },
  viewAllUserViewPage: {
    flex: 1,
    backgroundColor: 'white',
  },
  buttonView: {
    margin: 20,
  },
});
