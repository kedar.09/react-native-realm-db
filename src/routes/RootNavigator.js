import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../pages/HomeScreen/HomeScreen';
import ViewUser from '../pages/ViewUser/ViewUser';
import ViewAllUser from '../pages/ViewAllUser/ViewAllUser';
import UpdateUser from '../pages/UpdateUser/UpdateUser';
import AddUser from '../pages/AddUser/AddUser';
import DeleteUser from '../pages/DeleteUser/DeleteUser';
import ViewUsersRange from '../pages/ViewUsersRange/ViewUsersRange';

const Stack = createStackNavigator();

export const RootNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="HomeScreen"
          component={HomeScreen}
          options={{title: 'Home Page'}}
        />

        <Stack.Screen
          name="ViewUser"
          component={ViewUser}
          options={{title: 'View User'}}
        />

        <Stack.Screen
          name="ViewAllUser"
          component={ViewAllUser}
          options={{title: 'View All User'}}
        />

        <Stack.Screen
          name="ViewUsersRange"
          component={ViewUsersRange}
          options={{title: 'View Users Between Range'}}
        />

        <Stack.Screen
          name="UpdateUser"
          component={UpdateUser}
          options={{title: 'Update User'}}
        />

        <Stack.Screen
          name="AddUser"
          component={AddUser}
          options={{title: 'Add New User'}}
        />

        <Stack.Screen
          name="DeleteUser"
          component={DeleteUser}
          options={{title: 'Delete User'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
